import { Observable } from 'rxjs';
import { ajax } from 'rxjs/ajax'
import  "./styles.css";

// const someObservable$ = new Observable<string>(subscriber => {
//   console.log('observable executed')
//   subscriber.next('Alice');
//   setTimeout(() => subscriber.next('Ben'), 1000);
//   setTimeout(() => subscriber.next('Charlie'), 4000);
//   // subscriber.complete();
// });
// console.log("subscription 1.1 starts")
// const subscription = someObservable$.subscribe(value => console.log('Subscription 1 ', value));

// setTimeout(() => {
// console.log("subscription 2 starts")
// const subscription = someObservable$.subscribe(value => console.log('Subscription 2 ', value));
// }, 1000);


// Cold Observables

const fetchObservable$ = ajax<any>('https://random-data-api.com/api/name/random_name')

const subscription1 = fetchObservable$.subscribe({
  next: (value) => console.log(value.response.four_word_name)
  }
)

const subscription2 = fetchObservable$.subscribe({
  next: (value) => console.log(value.response.four_word_name)
  }
)

// Hot observables

const button = document.querySelector('#button1')

const hotObservable$ = new Observable<MouseEvent>(subscriber => {
    button.addEventListener('click', (event: MouseEvent) => {
        subscriber.next(event)
    })
})

hotObservable$.subscribe(
  event => console.log('sub1 ', event.type, event.x, event.y)
)

hotObservable$.subscribe(
  event => console.log('sub2 ', event.type, event.x, event.y)
)